use std::sync::{Arc, Mutex};

use smoltcp::{phy::{Device, DeviceCapabilities, Loopback, RxToken, TxToken}, socket::TcpState};
use smoltcp::wire::{EthernetAddress, IpAddress, IpCidr};
use smoltcp::iface::{NeighborCache, EthernetInterfaceBuilder};
use smoltcp::socket::{SocketSet, TcpSocket, TcpSocketBuffer};
use smoltcp::time::{Duration, Instant};
use simple_logger::SimpleLogger;
use smoltcp::{wire::{EthernetFrame, TcpRepr, TcpPacket, Ipv4Repr, Ipv4Packet, IpProtocol}, phy::ChecksumCapabilities};
use log::{debug, trace};

const TESTBUFFER: &'static [u8] = b"
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac feugiat sapien, et condimentum quam. Phasellus nec faucibus nulla. Vivamus at bibendum mauris. Aliquam erat volutpat. Ut rutrum arcu sed nisi efficitur, ac sagittis lacus bibendum. Phasellus et ante lectus. Integer vestibulum, dolor consequat scelerisque fermentum, neque neque ultrices dolor, nec ultrices nisi lectus sit amet tellus. Morbi scelerisque quis mi nec facilisis. Morbi porttitor tortor rhoncus mauris blandit sodales. Etiam elementum nunc a laoreet vulputate. Curabitur interdum imperdiet viverra.
Donec elementum porta risus, in commodo libero sodales eget. Suspendisse porttitor vitae mi ut volutpat. Proin eget accumsan neque. Vestibulum quis ex placerat, semper odio in, lobortis nulla. Nulla sollicitudin eget tortor sed venenatis. Phasellus eget purus ligula. Nam fringilla ultrices dapibus. Vivamus iaculis elementum lectus id malesuada.
Nulla ultrices erat et vulputate varius. Pellentesque nec massa viverra, pharetra nulla a, rutrum 
";

mod mock {
    use smoltcp::time::{Duration, Instant};
    use core::cell::Cell;

    #[derive(Debug)]
    pub struct Clock(Cell<Instant>);

    impl Clock {
        pub fn new() -> Clock {
            Clock(Cell::new(Instant::from_millis(0)))
        }

        pub fn advance(&self, duration: Duration) {
            self.0.set(self.0.get() + duration)
        }

        pub fn elapsed(&self) -> Instant {
            self.0.get()
        }
    }
}

struct MyTxToken(Arc<Mutex<Vec<MyRxToken>>>);
impl TxToken for MyTxToken{
    fn consume<R, F>(self, timestamp: Instant, len: usize, f: F) -> smoltcp::Result<R>
    where F: FnOnce(&mut [u8]) -> smoltcp::Result<R> {
        let mut new_vec = vec![0 as u8; len];
        let res = f(&mut new_vec);

        if res.is_ok(){
            if let Ok(ethframe) = EthernetFrame::new_checked(&new_vec){
                if let Ok(ippkg) = Ipv4Packet::new_checked(ethframe.payload()){
                    if let Ok(tcppkg) = TcpPacket::new_checked(ippkg.payload()){
                        if tcppkg.payload().len() > 0{
                            //print!("{}", core::str::from_utf8(&tcppkg.payload()).unwrap_or("UNPARSED"));
                        }
                    }else{
                        //debug!("Could not parse tcp");
                    }
                }else{
                    //println!("Could not parse ip tx");
                }
            }else{
                //println!("Failed to parse ethernet frame tx");
            }
        }
        self.0.lock().unwrap().push(MyRxToken(new_vec));
        res
    }
}
struct MyRxToken(Vec<u8>);
impl RxToken for MyRxToken{
    fn consume<R, F>(mut self, timestamp: Instant, f: F) -> smoltcp::Result<R>
    where F: FnOnce(&mut [u8]) -> smoltcp::Result<R> {
        
        let parsed = if let Ok(ethframe) = EthernetFrame::new_checked(&self.0){
            if let Ok(ippkg) = Ipv4Packet::new_checked(ethframe.payload()){
                if let Ok(tcppkg) = TcpPacket::new_checked(ippkg.payload()){
                    let s = core::str::from_utf8(&tcppkg.payload()).unwrap_or("UNPARSED");
                    s.to_string()
                }else{
                    "TCPUNPARSED".to_string()
                }
            }else{
                "IPUNPARSED".to_string()
            }
        }else{
            "ETHUNPARSED".to_string()
        };

        //Actual recv
        let res = f(&mut self.0);
        if res.is_ok() && parsed.len() > 0{
           // print!("[RECV]{}[RECV]", parsed);
        }
        res
    }
}

#[derive(Clone)]
struct SharedLoop{
    send: Arc<Mutex<Vec<MyRxToken>>>,
    recv: Arc<Mutex<Vec<MyRxToken>>>
}

impl SharedLoop{
    pub fn new() -> (Self, Self){
        let a = Arc::new(Mutex::new(Vec::new()));
        let b = Arc::new(Mutex::new(Vec::new()));

        (SharedLoop{send: a.clone(), recv: b.clone()}, SharedLoop{send: b, recv: a})
    }
}

impl<'a> Device<'a> for SharedLoop{
    type TxToken = MyTxToken; 
    type RxToken = MyRxToken;
    fn capabilities(&self) -> DeviceCapabilities {
        let mut caps = DeviceCapabilities::default();
        caps.max_transmission_unit = 256;
        caps
    }
    fn transmit(&'a mut self) -> Option<Self::TxToken> {
        Some(MyTxToken(self.send.clone()))
    }
    fn receive(&'a mut self) -> Option<(Self::RxToken, Self::TxToken)> {

        let mut lck = self.recv.lock().unwrap();
        if lck.len() > 0{
            let front = lck.remove(0);
            Some((front, MyTxToken(self.send.clone())))
        }else{
            None
        }
    }
}

fn lo(is_sender: bool, device: SharedLoop, crash_flag: Arc<Mutex<bool>>){
    let clock = mock::Clock::new();
    let mut neighbor_cache_entries = [None; 8];
    let neighbor_cache = NeighborCache::new(&mut neighbor_cache_entries[..]);

    let ip_addrs = if is_sender {
        [IpCidr::new(IpAddress::v4(127, 0, 0, 1), 8)]
    }else{
        [IpCidr::new(IpAddress::v4(127, 0, 0, 2), 8)]
    };
    
    let mut iface = EthernetInterfaceBuilder::new(device)
            .ethernet_addr(EthernetAddress::default())
            .neighbor_cache(neighbor_cache)
            .ip_addrs(ip_addrs)
        .finalize();
    
    const buffer_size: usize = 16 * 1024;
    let socket = {
        // It is not strictly necessary to use a `static mut` and unsafe code here, but
        // on embedded systems that smoltcp targets it is far better to allocate the data
        // statically to verify that it fits into RAM rather than get undefined behavior
        // when stack overflows.
        static mut TCP_SERVER_RX_DATA: [u8; buffer_size] = [0; buffer_size];
        static mut TCP_SERVER_TX_DATA: [u8; buffer_size] = [0; buffer_size];
        let tcp_rx_buffer = TcpSocketBuffer::new(unsafe { &mut TCP_SERVER_RX_DATA[..] });
        let tcp_tx_buffer = TcpSocketBuffer::new(unsafe { &mut TCP_SERVER_TX_DATA[..] });
        TcpSocket::new(tcp_rx_buffer, tcp_tx_buffer)
    };

    let mut socket_set_entries: [_; 1] = Default::default();
    let mut socket_set = SocketSet::new(&mut socket_set_entries[..]);
    let handle = socket_set.add(socket);

    {
        let mut socket = socket_set.get::<TcpSocket>(handle);
        //Connect
        if is_sender{
            socket.connect((IpAddress::v4(127, 0, 0, 2), 1234),
                           (IpAddress::Unspecified, 65000)).unwrap();
            
        }else{
            socket.listen(1234).unwrap();
        }
    }

    let mut num_packages = 0;

    
    let mut buffer = vec![0 as u8; 2048];
    let mut send_ptr = 0;
    'outer: loop{

        if *crash_flag.lock().unwrap(){
            return;
        }
        
        //println!("Poll[{}]", is_sender);
        match iface.poll(&mut socket_set, clock.elapsed()) {
            Ok(_) => {},
            Err(e) => {
                println!("poll error: {}", e);
            }
        }
        
        //println!("work[{}]", is_sender);
        {
            let mut socket = socket_set.get::<TcpSocket>(handle);
            if socket.state() == TcpState::Established{
                if is_sender{
                    let size = socket.send_slice(&TESTBUFFER[send_ptr..]).unwrap();

                    if size > 0{
                        println!("Send [{}..{}]", send_ptr, (send_ptr+size));
                    }
                    send_ptr += size;

                    if send_ptr >= TESTBUFFER.len(){
                        send_ptr = 0;
                    }
                }else{
                    let mut ptr = 0;
                    while ptr < TESTBUFFER.len(){
                        match socket.recv_slice(&mut buffer[ptr..1024]){
                            Ok(recv_size) => {
                                if recv_size > 0{
                                    //println!("RECVED!");
                                }else{
                                    //println!("NOT RECV");
                                    continue 'outer;
                                }
                                ptr += recv_size
                            },
                            Err(e) => println!("Err: {}", e),
                        }
                        //println!("@{}", ptr);
                    }
                    //println!("je");

                    if &buffer[0..ptr] != TESTBUFFER{
                        println!("Crashed on assert!");
                        *crash_flag.lock().unwrap() = true;
                    }
                    
                    assert!(
                        &buffer[0..ptr] == TESTBUFFER, "Does not assert, was: \n{}\nshould be:\n{}\n\nThis was the package {} on the {}th buffer size",
                        core::str::from_utf8(&buffer[0..1024]).unwrap_or("UNPARSED"),
                        core::str::from_utf8(&TESTBUFFER).unwrap_or("UNPARSED"),
                        num_packages,
                        num_packages as f64 / buffer_size as f64
                    );
                    num_packages += 1;
                }
            }else{
                println!("Not Connected[{}]", is_sender);
            }
        }
        
        //println!("Poll time[{}]", is_sender);
        match iface.poll_delay(&socket_set, clock.elapsed()) {
            Some(Duration { millis: 0 }) => {}, //println!("resuming"),
            Some(delay) => {
                //println!("sleeping for {} ms", delay);
                clock.advance(delay)
            },
            None => clock.advance(Duration::from_millis(10))
        }
        //std::thread::sleep(std::time::Duration::from_millis(1000));
    }
    
    
}


fn main() {

    SimpleLogger::new().init().unwrap();
    let is_crashed = Arc::new(Mutex::new(false));
    let icc = is_crashed.clone();
    let (d1, d2) = SharedLoop::new();
    let client = std::thread::spawn(||lo(true, d1, icc));
    let server = std::thread::spawn(move||lo(false, d2, is_crashed));

    client.join().unwrap();
    server.join().unwrap();
}
